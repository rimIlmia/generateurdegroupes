const express = require("express");
const bodyParser = require("body-parser");
const routes = require("./routes/routes");
const mongoose = require("mongoose");

//set up express application
const app = express();
//connect to mongodb
mongoose.connect(
  "mongodb://localhost:27017/ma_base",
  {
    useNewUrlParser: true
  },
  function(error, client) {
    if (error) throw error;
    console.log("connexion ok");
  }
);
mongoose.Promise = global.Promise;
let port = process.env.port || 4000;

app.use(bodyParser.json());

//use routes
app.use(routes);
/* ou bien ne pas declarer et initialiser routes et le faire au mme temps de cette façon app.use("/routes",require("./routes/routes"))*/
//listen for requests
app.listen(
  port,
  () => console.log(`Serveur fonctionne sur le port ${port}`) //interpolation
);
