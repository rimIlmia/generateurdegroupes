const mongoose = require("mongoose");
const Schema = mongoose.Schema;
//create schema
const userSchema = new Schema({
  nom: {
    type: String,
    required: [true, "Le nom est obligatoire"]
  }
});
//create Model
const user = mongoose.model("personnes", userSchema);
module.exports = user;
