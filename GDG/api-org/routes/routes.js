const express = require("express");
const router = express.Router();
const User = require("../models/user");

//Ajouter un utilisateur à la DB
router.post("/addUser", (req, res) => {
  User.create(req.body).then(user => {
    res.json(user);
    console.log(user);
  });
});
//Liste users
router.get("/users", (req, res) => {
  User.find({}).then(function(user) {
    res.send(user);
  });
});
//Generation de groupes
router.get("/test", (req, res) => {
  function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }
  function decoupage(tableau, element) {
    let total = [];
    let b = 0;
    while (tableau.length - element >= 0) {
      let result = [];
      for (let i = 0; i < element; i++) {
        let extract = getRandomArbitrary(0, tableau.length);
        result.push(tableau[extract]);
        tableau.splice(extract, 1);
      }
      total.push(result);
    }
    return total;
  }
  User.find({}).then(function(usersArray) {
    res.json(decoupage(usersArray, 2));
  });
});
module.exports = router;
