//L'application requiert l'utition d'express pour gerer les routes
const express = require("express");
//L'application requiert l'utilisation de mongoClient pour acceder à la BD
const MongoClient = require("mongodb").MongoClient;
//L'application a besoin de body_parser pour parser les donnees
const bodyParser = require("body-parser");
// Les paramètres du serveur.
var hostname = "localhost";
var port = 3000;
//creation d'un objet express=== serveur
var api = express();
//utilisation de bodyParser pour parser les données passées dans la requete
api.use(bodyParser.urlencoded({ extended: false }));
api.use(bodyParser.json());
// le chemin
api.use(express.static(__dirname + "/"));

/****************************Definition des routes de l'API****************/
// test index
api.get("/index", (req, res) => {
  res.render("index.html");
});

() => {
  console.log("");
};

//nouveau utilisateur
api.post("/addUser", function(req, resp) {
  MongoClient.connect(
    "mongodb://localhost:27017/ma_base",
    { useNewUrlParser: true },
    function(error, client) {
      if (error) throw error;
      console.log("connexion ok");
      var db = client.db("ma_base");
      //insertion d'un nouveau user
      var myobj = { nom: req.body.nom };
      console.log(`welcome ${req.body.nom}`); //interpolation
      db.collection("personnes").insertOne(myobj, function(err, res) {
        if (err) throw err;
        console.log("Une personne ajoutée avec succes");
        db.collection("personnes")
          .find({})
          .toArray(function(err, result) {
            if (err) throw err;
            console.log(result);

            resp.send(result);
            client.close();
          });
      });
    }
  );
});

api.get("/users", function(req, res) {
  MongoClient.connect(
    "mongodb://localhost:27017/ma_base",
    { useNewUrlParser: true },
    function(error, client) {
      if (error) throw error;
      console.log("connexion ok");

      var db = client.db("ma_base");

      db.collection("personnes")
        .find({})
        .toArray(function(err, result) {
          if (err) throw err;
          console.log(result);
          res.send(result);
        });

      client.close();
    }
  );
});
api.listen(port, hostname, function() {
  console.log("Mon serveur API fonctionne sur http://" + hostname + ":" + port);
});
